section .data
	userMsg db 'Please enter a number: '
	lenUserMsg equ $-userMsg
	dispMsg db 'You have entered: '
	lenDispMsg equ $-dispMsg

section .bss
	num resb 5
section .text
	global _start
_start:
	mov eax, 4 ;prompt
	mov ebx,1
	mov ecx, userMsg
	mov edx, lenUserMsg
	int 80h

	;read
	mov eax,3
	mov ebx, 2
	mov ecx,num
	mov edx, 5
	 ;5 byts
	int 80h

	;output msg
	mov eax,4
	mov ebx,1
	mov ecx, dispMsg
	mov edx, lenDispMsg
	int 80h

	;output #
	mov eax,4
	mov ebx,1
	mov ecx,num
	mov edx, 5
	int 80h
	;exit
	mov eax,1
	mov ebx,0
	int 80h

