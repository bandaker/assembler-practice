#! /bin/bash

clear

echo "Assembling and running $1"
nasm -f elf $1.asm
ld -m elf_i386 -s -o run $1.o
echo ""
echo ""
./run 
echo ""
echo ""
